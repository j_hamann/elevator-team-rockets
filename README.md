# Elevator
Elevate your goals!

## Production Site
Visit our site at:

https://elevator-4bb03.firebaseapp.com

Or run the following steps to run the site locally

## Install stuff
`yarn` to install dependencies

## Run the app
`yarn start` to run the app.

## Lint code
`yarn lint` to lint your code.


Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
