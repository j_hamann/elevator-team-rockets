# thinking about our data representation
It will be beneficial longer term to have direction to our data,
especially since we have a (comparatively to relational db) unstructured backend.

#### "Garbage in, garbage out" - NoSql without direction


## Goals
- goalName
- Goal stated as a question ("DId you play the lute today?")
- goal description 
- Minimal Actionable Task 
- category 
- motivators = [string] (list of 5 reasons why you want to achieve your goal. Use these in push notifs)
- color
- public (bool)
- progress
    - timestamp
    - successful? (bool)
- reminder
    - timestamp to remind
    - persistent?
    - days per week
- owner (unsure)
    
    
## MVP operations this data allows
- Goal creation
- list goals
- derive graphs of progress and streaks
- construct leaderboard of public goals in a given category
- motivation push notifs
- push to create progress log
- Calculate habit strength
- List goals with color


