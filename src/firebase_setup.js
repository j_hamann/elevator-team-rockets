import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import {useEffect, useState} from "react";
import moment from "moment";

// here are our credentials idgaf
const firebaseConfig = {
    apiKey: "AIzaSyALKZ7Ep6ZDntjlXzirQCZX9e0D074Cjck",
    authDomain: "elevator-4bb03.firebaseapp.com",
    databaseURL: "https://elevator-4bb03.firebaseio.com",
    projectId: "elevator-4bb03",
    storageBucket: "elevator-4bb03.appspot.com",
    messagingSenderId: "183876051706",
    appId: "1:183876051706:web:a4057f70a26c3a5b735f43",
    measurementId: "G-XLLK69Q7T3"
};


class Firebase {
    constructor() {
        firebase.initializeApp(firebaseConfig);
        this.auth = firebase.auth();
        this.db = firebase.firestore();
        this.provider = new firebase.auth.GoogleAuthProvider();
        moment.locale("en");
    }

    login() {
        return this.auth.signInWithRedirect(this.provider)
    }

    signout() {
        return this.auth.signOut()
    }

    isInitialised() {
        return new Promise(resolve => {
            this.auth.onAuthStateChanged(resolve)
        })
    }

    getCurrentUserName() {
        console.log(this.auth.currentUser.photoURL)
        return this.auth.currentUser.displayName
    }

    getPhotoUrl() {
        return this.auth.currentUser.photoURL
    }

    getCurrentUserId() {
        return this.auth.currentUser.uid
    }

    getUserPhoto() {
        return this.auth.currentUser.photoURL
    }

    isUserLoggedIn() {
        const user = this.auth.currentUser;
        return !!user;
    }

    getCreationTime() {
      return this.auth.currentUser.metadata.creationTime;
    }

    getLastSignInTime() {
      return this.auth.currentUser.metadata.lastSignInTime;
    }

    getUserEmail() {
      return this.auth.currentUser.email;
    }

    deleteUser() {
      return this.auth.currentUser.delete();
    }

    // FireStore methods
    // Add a goal to the "goals" collection.
    addGoal(goalFields) {
        const d = new Date();
        const now = new Date(d.getFullYear(), d.getMonth(), d.getDate());
        return this.db
            .collection("goals")
            .add({
                ownerId: this.auth.currentUser.uid,
                ownerName: this.auth.currentUser.displayName,
                ownerImage: this.auth.currentUser.photoURL,
                creationDate: now,
                progress: {

                }, // empty at start
                ...goalFields
            });

    }

    updateGoal(goalId, goalFields) {
        return this.db
            .collection("goals")
            .doc(goalId)
            .update(goalFields);
    }

    // we also may want to do something like this to load the goals in a view component
    useGoals() {
        const [goals, setGoals] = useState([null]);

        useEffect(() => {
            const unsub = this
                .db
                .collection('goals')
                .where("ownerId", "==", this.auth.currentUser.uid) // this should also have firebase secuirty rule enforcement
                .onSnapshot((snapshot) => {
                    const newGoals = snapshot.docs.map((doc) => ({
                            id: doc.id,
                            ...doc.data()
                        })
                    );
                    setGoals(newGoals)
                });
            return () => unsub()
        }, []);

        return goals;
    }


    useGoalCategories() {
        const [categories, setCategories] = useState([]);

        useEffect(() => {
           const unsub = this
               .db
               .collection("categories")
               .onSnapshot((snapshot) => {
                  const newCategories = snapshot.docs.map((doc) => ({
                      id: doc.id,
                      ...doc.data()
                  }));
                  setCategories(newCategories);
               });

            return () => unsub()
        }, []);

        return categories;
    }


    getStreakDays(progress) {
        if (!progress) return 0;

        // iterate back from current date to get streak
        var d = new Date();
        d = new Date(d.getFullYear(), d.getMonth(), d.getDate())

        var streak = 0;
        while (progress[d]) {
            streak = streak + 1;
            d.setDate(d.getDate() - 1)
        }
        return streak
    }


    // given a progress array and a start date, converts to a list of values
    // `undefined' ≡ no progress, `false' ≡ fail, `true' ≡ success
    progressToArray(creationDate, progress) {
        const ret = [];
        if (!creationDate) {
          return ret;
        }

        let d;
        if (creationDate.hasOwnProperty('seconds')) { // eslint-disable-line
            d = new Date(creationDate.seconds * 1000);
        } else {
            d = new Date(creationDate);
        }

        const now = new Date();
        while (d < now) {
            if (progress) {
                ret.push(progress[d]);
            } else {
                ret.push(undefined);
            }
            d.setDate(d.getDate() + 1);
        }

        return ret;
    }


    getPublicCategoryGoals(category) {
      const [streaks, setStreaks] = useState([null]);
      useEffect(() => {
         const unsub = this
             .db
             .collection("goals")
             .where("public", '==', true)
             .where("category", '==', category)
             .onSnapshot((snapshot) => {
                const newGoals = snapshot.docs.map((doc) => ({
                    id: doc.id,
                    ...doc.data()
                }));
                setStreaks(newGoals);
             });

          return () => unsub()
      }, [category]);
      return streaks;
    }

    getUserPublicCategoryGoals(userId, category) {
      const [streaks, setStreaks] = useState([]);
      useEffect(() => {
         const unsub = this
             .db
             .collection("goals")
             .where("public", '==', true)
             .where("category", '==', category)
             .where("ownerId", '==', userId)
             .onSnapshot((snapshot) => {
                const newGoals = snapshot.docs.map((doc) => ({
                    id: doc.id,
                    ...doc.data()
                }));
                setStreaks(newGoals);
             });

          return () => unsub()
      }, [category, userId]);
      return streaks;
    }

    // gets 3 random goals that are not owned by the current user
    getRandomGoals(category) {
      const [goals, setGoals] = useState([]);

      // get all public goals
      useEffect(() => {
          const pubGoals = this
              .db
              .collection("goals")
              .where("public", '==', true)
              .onSnapshot((snapshot) => {
                  const newGoals = snapshot.docs.map((doc) => ({
                      id: doc.id,
                      ...doc.data()
                  }));
                  setGoals(newGoals);
              });
              return () => pubGoals()
      }, [category]);

      // shuffle goals
      var currentIndex = goals.length, temporaryValue, randomIndex;
      while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = goals[currentIndex];
          goals[currentIndex] = goals[randomIndex];
          goals[randomIndex] = temporaryValue;
      }

      // grab first 3 goals in shuffled array
      var result = [];
      var goalsLen = goals.length
      for (var i = 0; i < goalsLen; i++) {
          if (goals[i]['ownerId'] !== this.getCurrentUserId()) {
              result.push(goals[i])
          }
          if (result.length === 3) break
      }

      return result;
    }


    deleteGoal(goalId) {
        return this.db
            .collection("goals")
            .doc(goalId)
            .delete();
    }

     addGoalProgress(id, successful) {
        // need to do it like this to have the key be dynamically generated to insert into map.
        var now = new Date();
        now = new Date(now.getFullYear(), now.getMonth(), now.getDate())
        const itemToLog = { };
        itemToLog[now] = successful;

        var newUpdate = {};
        const key = `progress.${now}`;
        newUpdate[key] = successful;

        return this.db
            .collection("goals")
            .doc(id)
            .update(newUpdate)
    }

    addGoalProgressDate(id, successful, logDate) {
      console.log("goalid", id);
       // need to do it like this to have the key be dynamically generated to insert into map.

       console.log("logDate", logDate);
       const itemToLog = { };
       itemToLog[logDate] = successful;

       var newUpdate = {};
       const key = `progress.${logDate}`;
       newUpdate[key] = successful;

       return this.db
           .collection("goals")
           .doc(id)
           .update(newUpdate)
   }

    useGoalsMonth(month, goalId) {
      const [daysInMonth, setDaysInMonth] = useState([]);

      useEffect(() => {
        const unsub = this
          .db
          .collection('goals')
          .where("ownerId", "==", this.auth.currentUser.uid)
          .onSnapshot((snapshot) => {
            const newDays = snapshot.docs.filter((doc) => {
              if(doc.id === goalId){
                return true
              }
              return false
            }).map((doc) => {
              let foundDays = [];
              Object.keys(doc.data().progress).forEach(key => {
                const date = moment(key);
                if (date.month() === month){
                  foundDays.push(date.date());
                }
              });
              return foundDays;
            });
            if (newDays && newDays.length !== 0){
              setDaysInMonth(newDays[0])
            }
          });
          return () => unsub()
      }, [month, goalId]);
      return daysInMonth;
    }

    // TODO - this only counts from earliest creation date, and assumes progress
    //  will never be logged before this date.
    getDataForDashGraph() {
      const [goals, setGoals] = useState([]);

      useEffect(() => {
          const unsub = this
              .db
              .collection('goals')
              .where("ownerId", "==", this.auth.currentUser.uid) // this should also have firebase secuirty rule enforcement
              .onSnapshot((snapshot) => {
                  const newGoals = snapshot.docs.map((doc) => ({
                          id: doc.id,
                          ...doc.data()
                      })
                  );
                  setGoals(newGoals)
              });
          return () => unsub()
      }, []);

      let earliestDate = null;
      for (let i = 1; i < goals.length; i++) {
          if (goals) {
              const loggedDates = Object.keys(goals[i].progress).map(dateStr => new Date(dateStr));
              loggedDates.forEach(d => { // eslint-disable-line
                  if (earliestDate === null) {
                      earliestDate = d;
                  }
                  if (d < earliestDate) {
                      earliestDate = d
                  }
              })
          }
          const creationDate = new Date(goals[i].creationDate.seconds * 1000);
          if (earliestDate === null) {
            earliestDate = creationDate
          } else if (creationDate < earliestDate) {
            earliestDate = creationDate
          }
      }

      if (earliestDate) {
          earliestDate = earliestDate.getTime();
      }

      console.log(earliestDate)

      // plot all days since earliest
      let counters = [[], []]
      if (goals.length > 0) {
        let tmpArray = this.progressToArray(earliestDate, goals[0]['progress'])
        // initialise data structure
        for (let i = 0; i < tmpArray.length; i++) {
          counters[0].push(moment(earliestDate).add(i, 'days').format('D MMM'))
          counters[1].push(0)
        }
        // populate data
        for (let i = 0; i < goals.length; i++) {
          let tmpArray = this.progressToArray(earliestDate, goals[i]['progress'])
          for (let j = 0; j < tmpArray.length; j++) {
            if (tmpArray[j] === true) {
              counters[1][j]++
            }
          }
        }
      }

      return counters
    }
}

export default new Firebase();
