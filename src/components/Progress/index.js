import React from "react"
import PropTypes from 'prop-types';

const Progress = (props) => {

    const { userName } = props;

    return  (
        <>
            <h3>{"Progress page I don't know lol"}</h3>
            <h4>Hello {userName}</h4>
            <h4>I have no idea what to put here yet. but ooo shiny tabs</h4>
        </>
    );
};

Progress.propTypes = {
    tab: PropTypes.number,
    userName: PropTypes.string,
};

export default Progress;
