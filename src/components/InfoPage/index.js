import React from 'react'
import Firebase from '../../firebase_setup';
import {Button} from "@material-ui/core";
import { Redirect } from "react-router-dom";
import '../Styles/_box-layout.css';

// TODO: remove when props are actually used
/* eslint-disable no-unused-vars */
const InfoPage = (props) => {
/* eslint-enable no-unused-vars */

    function login() {
        try {
            Firebase.login();
        } catch (e) {
            // alert for now
            alert(e);
        }
    }

    if (Firebase.isUserLoggedIn()) {
        return <Redirect to='/dashboard' />
    }

    return (
        <>
            <div className="box-layout">
              <div className="box-layout__box">
                <h1>Elevator Goals</h1>
                <h3>Time to elevate your life!</h3>
                <p>{"Please login to use Elevator"}</p>
                <br />
                <p>
                <Button variant="contained" color="primary"  onClick={login} >
                    Login with Google
                </Button>
                </p>
                <p>
                <Button variant="contained" color="primary"  onClick={login} >
                    Register with Google
                </Button>
                </p>
              </div>
            </div>
        </>
    )
};

// TODO when we pass in props here
InfoPage.propTypes = {};

export default InfoPage;
