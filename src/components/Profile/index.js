import React, {useState} from "react";
import PropTypes from 'prop-types'

import Firebase from "../../firebase_setup"
import { makeStyles } from '@material-ui/core/styles'

import Button             from "@material-ui/core/Button/Button";
import Checkbox           from '@material-ui/core/Checkbox';
import Dialog             from '@material-ui/core/Dialog';
import DialogActions      from '@material-ui/core/DialogActions';
import DialogContent      from '@material-ui/core/DialogContent';
import DialogContentText  from '@material-ui/core/DialogContentText';
import DialogTitle        from '@material-ui/core/DialogTitle';
import Fade               from '@material-ui/core/Fade';
import FormControlLabel   from '@material-ui/core/FormControlLabel';
import FormControl        from '@material-ui/core/FormControl';
import FormGroup          from '@material-ui/core/FormGroup';
import FormLabel          from '@material-ui/core/FormLabel';
import IconButton         from '@material-ui/core/IconButton';
import List               from '@material-ui/core/List';
import ListItem           from '@material-ui/core/ListItem';
import ListItemText       from '@material-ui/core/ListItemText';
import Radio              from '@material-ui/core/Radio';
import RadioGroup         from '@material-ui/core/RadioGroup';
import Slide              from '@material-ui/core/Slide';
import Snackbar           from "@material-ui/core/Snackbar";
import SnackbarContent    from '@material-ui/core/SnackbarContent';
import Tooltip            from '@material-ui/core/Tooltip';

import CloseIcon               from '@material-ui/icons/Close';
import DeleteForeverIcon       from '@material-ui/icons/DeleteForever';
import InfoIcon                from '@material-ui/icons/Info';
import RestoreIcon             from '@material-ui/icons/Restore';
import SaveIcon                from '@material-ui/icons/Save';

import { green }    from '@material-ui/core/colors';


const useStyles = makeStyles(theme => ({
  root: {

  },
  profile: {
    width: '264px',
    marginLeft: '50px',
    marginRight: '50px',
    borderRadius: '10%',
  },
  button: {
      margin: theme.spacing(1),
      alignItems: 'normal',
      paddingTop: '10px',
      lineHeight: "1.3",
  },
}))

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const snackbarVariantIcon = {
    info: InfoIcon,
};

export default function Profile(props) {
  document.title = "My Profile | Elevator Goals";

  const classes = useStyles()
  const { userName } = props

  let initNotifFreq = "daily";
  let initNotifLocation = {
    pc: false,
    mobile: false,
  };
  const [notifFreq, setNotifFreq] = React.useState(initNotifFreq);
  const [notifLocation, setNotifLocation] = React.useState(initNotifLocation);
  const {pc, mobile} = notifLocation;
  const [deletePopup, setDeletePopup] = React.useState(false);

  const handleNotifFreqChange = event => {
    setNotifFreq(event.target.value);
  };

  const handleNotifLocationChange = name => event => {
    setNotifLocation({ ...notifLocation, [name]: event.target.checked });
  };

  async function deleteAccount() {
    setDeletePopup(false);
    try {
        await Firebase.deleteUser();
        window.location.reload()
    } catch (e) {
        alert(e.message)
    }
  }

  // Snackbar
  const [snackbarStatus, setSnackbarStatus] = useState(null);
  const [snackbarMessage, setSnackbarMessage] = useState("");

  function SnackbarWrapper(props) {
      const {message, variant, ...other} = props;
      const Icon = snackbarVariantIcon[variant];

      return (
          <SnackbarContent
              style={{backgroundColor: green[600]}}
              className={classes[variant]}
              message={
                  <span id="snackbar" style={{display: 'flex', alignItems: 'center'}}>
                  <Icon style={{paddingRight: "15px"}}/>
                  {message}
              </span>
              }
              action={[
                  <IconButton key="close" aria-label="close" color="inherit" onClick={() => setSnackbarStatus(null)}>
                      <CloseIcon/>
                  </IconButton>,
              ]}
              {...other}
          />
      );
  }

  const settingsSnackbar = variant => {
      const mapping = {
          "revert": {
              "variant": "info",
              "message": `Settings reverted`,
          },
          "saved": {
              "variant": "info",
              "message": `Settings saved`,
          }
      }

      setSnackbarStatus(mapping[variant].variant);
      setSnackbarMessage(mapping[variant].message);
  };

  function saveSettings() {
    settingsSnackbar("saved");
    initNotifFreq = notifFreq;
    initNotifLocation = notifLocation;
  }

  function revertSettings() {
    settingsSnackbar("revert");
    setNotifFreq(initNotifFreq);
    setNotifLocation(initNotifLocation);
  }

  return (
    <div>
      {snackbarStatus &&
          <Snackbar
            open={!!snackbarStatus}
            autoHideDuration={1000}
            onClose={() => setSnackbarStatus(null)}
            anchorOrigin={{'vertical': 'top', 'horizontal': 'center'}}
            TransitionComponent={Fade}
          >
              <SnackbarWrapper variant={snackbarStatus} message={snackbarMessage} />
          </Snackbar>
      }

      <h1>{userName}</h1>
      <div className="dashboardRow">
          <div className="dashboardColumn">
            <img className={classes.profile}
              alt='User Profile'
              src={Firebase.getPhotoUrl()}/>
          </div>
          <div className="dashboardColumn">
            <h2 style={{textAlign: "center", margin: "20px 0px 0px 30px"}}>User Information</h2>
            <List>
              <ListItemText primary="Email:" secondary={Firebase.getUserEmail()}/>
              <ListItemText primary="Creation Date:" secondary={Firebase.getCreationTime()}/>
              <ListItemText primary="Last Sign In:" secondary={Firebase.getLastSignInTime()}/>
            </List>
          </div>
      </div>

      <br/>

      <div style={{marginBottom: "40px"}}>
        <div style={{display: 'flex'}}>
          <h2 style={{textAlign: "left", margin: "20px 0px 0px 0px"}}>Notification Settings</h2>
        </div>
        <List style={{marginLeft: "20px"}}>
          <ListItem>
            <FormControl component="fieldset">
              <FormLabel component="legend">Notification Frequency</FormLabel>
              <RadioGroup aria-label="position" name="position" value={notifFreq} onChange={handleNotifFreqChange} row>
                <FormControlLabel
                  value="hourly"
                  control={<Radio color="primary" />}
                  label="Hourly"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="daily"
                  control={<Radio color="primary" />}
                  label="Daily"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="weekly"
                  control={<Radio color="primary" />}
                  label="Weekly"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="monthly"
                  control={<Radio color="primary" />}
                  label="Monthly"
                  labelPlacement="end"
                />
              </RadioGroup>
            </FormControl>
          </ListItem>
          <ListItem>
            <FormControl component="fieldset">
              <FormLabel component="legend">Notification Location</FormLabel>
              <FormGroup aria-label="position" row>
                <FormControlLabel
                  control={<Checkbox checked={pc} onChange={handleNotifLocationChange('pc')} color="primary" value="pc"/>}
                  label="PC"
                  labelPlacement="end"
                />
                <FormControlLabel
                  control={<Checkbox checked={mobile} onChange={handleNotifLocationChange('mobile')} color="primary" value="mobile"/>}
                  label="Mobile"
                  labelPlacement="end"
                />
              </FormGroup>
            </FormControl>
          </ListItem>
        </List>
        <Tooltip title="Save Notification Settings">
          <Button
            onClick={() => (saveSettings())}
            variant="contained"
            color="primary"
            startIcon={<SaveIcon/>}
            className={classes.button}
             >
              <b>Save</b>
          </Button>
        </Tooltip>
        <Tooltip title="Revert Notification Settings">
          <Button
            onClick={() => (revertSettings())}
            variant="contained"
            color="primary"
            startIcon={<RestoreIcon/>}
            className={classes.button}
             >
              <b>Revert</b>
          </Button>
        </Tooltip>

        <Tooltip title="Permanently delete your account">
          <Button
            onClick={() => setDeletePopup(true)}
             variant="contained"
             startIcon={<DeleteForeverIcon />}
             className={classes.button}
             style={{
                backgroundColor: "rgb(220, 0, 30)",
                float: 'right',
              }}>
              <b>Delete Account</b>
          </Button>
        </Tooltip>
      </div>


    { deletePopup ? (
        <Dialog
            open={deletePopup}
            TransitionComponent={Transition}
            onClose={() => setDeletePopup(false)}
            aria-labelledby="Delete Account"
        >
          <DialogTitle>
              Delete Account
          </DialogTitle>
          <DialogContent>
              <DialogContentText id="alert-dialog-description">
                  Are you sure you want to <i>permanently</i> delete your account?
              </DialogContentText>
              <DialogContentText
                id="alert-dialog-description-confirmation"
                style={{textAlign: 'center'}}
              >
                  <b>This action is irreversible</b>
              </DialogContentText>
          </DialogContent>
          <DialogActions>
              <Button onClick={() => setDeletePopup(false)} color="primary" autoFocus>
                  Cancel
              </Button>
              <Button onClick={() => deleteAccount()} color="primary" autoFocus>
                  Delete
              </Button>
          </DialogActions>
        </Dialog>
    ) : null }
    </div>
  );
}


Profile.propTypes = {
  userName: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
}
