import React, {useState} from "react"

import Firebase from "../../firebase_setup"

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import Icon from '@material-ui/core/Icon';
import InputLabel from '@material-ui/core/InputLabel';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import StarIcon from '@material-ui/icons/Star';
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import Avatar from "@material-ui/core/Avatar/Avatar";

import {
    FacebookIcon,
    FacebookShareButton,
    TwitterIcon,
    TwitterShareButton,
} from 'react-share';

function desc(a, b) {
    const orderBy = 'streak';
    if (b[orderBy] < a[orderBy]) {
        return -1
    }
    if (b[orderBy] > a[orderBy]) {
        return 1
    }
    return 0
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index])
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0])
        if (order !== 0) return order
        return a[1] - b[1]
    })
    return stabilizedThis.map(el => el[0])
}

function getSorting() {
    return (a, b) => desc(a, b)
}

const Leaderboard = () => {
    document.title = "Leaderboard | Elevator Goals";

    const [category, setCategory] = useState("Fitness")
    const goals = Firebase.getPublicCategoryGoals(category);


    const handleChangeCategory = event => {
        setCategory(event.target.value)
    };

    const currentUserId = Firebase.getCurrentUserId();
    const streaks = goals.filter(g => g !== null).map((goal) =>
        ({
            id: goal.id,
            ownerName: goal.ownerName,
            ownerId: goal.ownerId,
            title: goal.title,
            streak: Firebase.getStreakDays(goal.progress),
            creationDate: goal.creationDate,
            description: goal.description,
            photoURL: goal.ownerImage || 'https://upload.wikimedia.org/wikipedia/commons/4/45/A_small_cup_of_coffee.JPG' // im using coffee as a placeholder until we delete goals and then all new goals will have an image by default
        })
    );

    return (
        <div>
            <div style={{display: 'flex', alignItems: 'baseline'}}>
                <h1 style={{ marginRight: '2rem' }}>Leaderboard:</h1>

                <FormControl>
                    <InputLabel shrink htmlFor="category-select">
                        Category
                    </InputLabel>
                    <Select
                        value={category}
                        onChange={handleChangeCategory}
                        inputProps={{
                            name: 'category',
                            id: 'category-select',
                        }}
                        displayEmpty
                        autoWidth={true}
                        name="category"
                        renderValue={value => value}
                    >
                        {Firebase.useGoalCategories().map(catchoice =>
                            (<MenuItem key={catchoice.id} value={catchoice.name}>
                              <ListItemIcon>{catchoice.icon !== null ? <Icon>{catchoice.icon}</Icon> : <StarIcon />}</ListItemIcon>
                              <ListItemText primary={catchoice.name} />
                             </MenuItem>)
                        )}
                    </Select>
                </FormControl>
            </div>

            <Table size="medium">
                <TableBody>
                    {!goals.includes(null)
                        ? (
                            stableSort(streaks, getSorting())
                                .slice(0, 10)
                                .map((row, rank) => {
                                    const streakDays = row.streak === 1 ? 'Day' : 'Days';
                                    const streakString = `${row.streak} ${streakDays}`;

                                    const leaderboardMessage = `I'm ranked #${rank + 1} with a streak of ${streakString.toLowerCase()} for ${category}!`;
                                    const websiteMessage = 'Elevate your goal tracking with Elevator.';
                                    const socialMessage = `${leaderboardMessage} ${websiteMessage}`;

                                    return (
                                        <TableRow
                                            tabIndex={-1}
                                            key={row.id}
                                        >
                                            <TableCell align="center">
                                                {rank === 0 ? (
                                                    <EmojiEventsIcon htmlColor='#4caf50'/>
                                                ) : (
                                                    <span style={{color: '#4caf50', fontWeight: 'bold'}}>{" "}{rank + 1}</span>
                                                )}
                                            </TableCell>
                                            <TableCell>
                                                <div style={{ display: 'flex'}}>
                                                    <Avatar alt="User Avatar" src={row.photoURL} />
                                                    <div style={{ paddingLeft: '10px'}}>
                                                        <h4 style={{ padding: 0, margin: 0}}>{row.ownerName}</h4>
                                                        <p style={{ padding: 0, margin: 0, textAlign: 'left'}}>{row.title}</p>
                                                    </div>
                                                </div>
                                            </TableCell>
                                            <TableCell align='right'>
                                                {streakString} <span role='img' aria-label='streak'>🔥</span>
                                            </TableCell>
                                            <TableCell>
                                                {row.ownerId === currentUserId && (
                                                    <div>
                                                        <Tooltip title="Share to Facebook">
                                                            <div>
                                                                <FacebookShareButton url={"https://elevator-4bb03.web.app/leaderboard"} quote={socialMessage}>
                                                                    <FacebookIcon size={32}/>
                                                                </FacebookShareButton>
                                                            </div>
                                                        </Tooltip>
                                                        <Tooltip title="Share to Twitter">
                                                            <div>
                                                                <TwitterShareButton url={"https://elevator-4bb03.web.app/leaderboard"} title={socialMessage}>
                                                                    <TwitterIcon size={32}/>
                                                                </TwitterShareButton>
                                                            </div>
                                                        </Tooltip>
                                                    </div>
                                                )}
                                            </TableCell>
                                        </TableRow>
                                    )
                                })
                        ) :
                        <div style={{display: 'flex', alignItems: "center", justifyContent: "center"}}>
                            <CircularProgress size={100}/>
                        </div>
                    }
                </TableBody>
            </Table>
        </div>
    )
}

export default Leaderboard;
