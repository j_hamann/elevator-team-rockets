import React, {useState} from "react";
import Firebase from '../../firebase_setup';
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';

const GoalInputModal = (props) => {
    const [goalInputValues, setGoalInputValues] = useState({...props.initialGoalInputValues});

    React.useEffect(() => {
        setGoalInputValues(props.initialGoalInputValues);
    }, [props.initialGoalInputValues]);

    const handleChange = name => event => {
        let value = event.target.value;
        if (name === 'public') { // RadioGroup uses strings to store its value for some reason - cast back to boolean
            value = event.target.value === "true";
        }
        setGoalInputValues({ ...goalInputValues, [name]: value });
    };

    const classes = {
        inputField: {
            marginRight: 15
        },
        formControl: {
            minWidth: 120
        }
    };

    // Wrapper around the saveAction passed in as a prop
    const wrappedSaveAction = () => {
        props.saveAction(goalInputValues);

        // Close modal
        props.setIsOpen(false);

        // Clear out title and description fields after submitting
        setGoalInputValues(props.initialGoalInputValues);
    };

    return (
        <Dialog
            open={props.isOpen}
            onClose={() => props.setIsOpen(false)}
            aria-labelledby={props.modalTitle}
        >
            {
                <>
                    <DialogTitle>
                        {props.modalTitle}
                    </DialogTitle>
                    <DialogContent>
                        <form style={{display: 'flex', flexWrap: 'wrap'}}>
                            <TextField fullWidth style={classes.inputField} id="goal-title" label="Title" value={goalInputValues.title} onChange={handleChange('title')} />
                            <TextField fullWidth style={classes.inputField} id="goal-description" label="Description" value={goalInputValues.description} onChange={handleChange('description')} />
                            <FormControl style={classes.formControl}>
                                <InputLabel htmlFor="goal-category">Category</InputLabel>
                                <Select fullWidth id="goal-category" value={goalInputValues.category} onChange={handleChange('category')}>
                                    {Firebase.useGoalCategories().map((category) =>
                                        (<MenuItem key={category.id} value={category.name}>{category.name}</MenuItem>)
                                    )}
                                </Select>
                            </FormControl>
                            <TextField fullWidth style={classes.inputField} id="goal-question" label="Goal as a question" value={goalInputValues.question} onChange={handleChange('question')} />
                            <TextField fullWidth style={classes.inputField} id="goal-min-task" label="Minimum Actionable Task" value={goalInputValues.minTask} onChange={handleChange('minTask')} />
                            <TextField fullWidth style={classes.inputField} id="goal-motivationOne" label="Motivation 1" value={goalInputValues.motivationOne} onChange={handleChange('motivationOne')} />
                            <TextField fullWidth style={classes.inputField} id="goal-motivationTwo" label="Motivation 2" value={goalInputValues.motivationTwo} onChange={handleChange('motivationTwo')} />
                            <TextField fullWidth style={classes.inputField} id="goal-motivationThree" label="Motivation 3" value={goalInputValues.motivationThree} onChange={handleChange('motivationThree')} />
                            <FormControl component="fieldset">
                              <RadioGroup aria-label="position" name="position" value={goalInputValues.public.toString()} onChange={handleChange('public')} row>
                                <FormControlLabel value="true" control={<Radio color="primary" />} label="Public" labelPlacement="start"/>
                                <FormControlLabel value="false" control={<Radio color="primary" />} label="Private" labelPlacement="start"/>
                              </RadioGroup>
                            </FormControl>
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => props.setIsOpen(false)} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={wrappedSaveAction} color="primary" autoFocus>
                            {props.saveTitle}
                        </Button>
                    </DialogActions>
                </>
            }
        </Dialog>
    );
};

GoalInputModal.propTypes = {
    initialGoalInputValues: PropTypes.object,
    modalTitle: PropTypes.string,
    saveTitle: PropTypes.string,
    saveAction: PropTypes.func,
    isOpen: PropTypes.bool,
    setIsOpen: PropTypes.func,
};

export default GoalInputModal;
