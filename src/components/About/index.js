import React from "react"

const About = () => {
  document.title = "About | Elevator Goals";

  return (
    <div>
      <h1>About</h1>
      <h2>Who are we?</h2>
      <p>Elevator ™ⓡⓒ2019 et al.</p>
      <h2>What are we?</h2>
      <p>Elevator is a goal-tracking application, where we assist you in creating good habits and elevating your lifestyle.</p>
      <p>To assist in this we provide a variety of methods of encouragement, including graphs and charts to help manage your progress, a &#34;streak&#34; system to encourage you to maintain a daily routine, public leaderboards to instigate you into performing better, a reminder of your motivations to help you continue when you&#39;re feeling down, social media integration, and much more!</p>
      <h2>I&#39;m in — how do I get started?</h2>
      <p>Just log in with your Google account.</p>
      <h2>How do I use the application?</h2>
      <p>To get started, press the green &#34;Add Goal&#34; button on the top of every page. This will walk you through creating your first goal. We recommend picking goals that you can make habits, to encourage regular routines and elevate your life in the most effective way possible.</p>
      <p>To log progress on your goals you can visit the <a href="/goals">goals</a> page. From here you may manage your existing goals, create new goals, and increase your streaks. Once a day you may press the &#34;+&#34; icon under &#34;Streak Progress&#34;. From here you will be asked if you completed the goal. This information will be used to update the streak counter for the goal — try to get as high a streak as possible!</p>
      <p>Once your goals are created, you can use your <a href="/dashboard">dashboard</a> to manage and view statistics on your goals as a whole. We also provide a carousel regularly iterating through the motivations behind your goals to keep you on track.</p>
      <h2>How do I know if my data is safe?</h2>
      <p>You can make your goals private, and nobody else will be able to see them. Most of your profile data and account information is stored with your Google account: we will never receive your password, or anything about you, other than your goals.</p>
      <h2>How can I manage my account?</h2>
      <p>Click on your avatar in the top-left corner. From here you may manage the information related to your local profile information, or sign out of the application.</p>
      <h2>Terms and conditions</h2>
      <p><em>(insert terms and conditions here once we have a lawyer!)</em></p>
    </div>
  );
};

export default About;
