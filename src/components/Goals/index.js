import React, {Fragment, useState} from "react";
import Firebase from '../../firebase_setup';
import PropTypes from 'prop-types';
import '../Styles/categories.css';
import '../Styles/_box-layout.css';


import GoalInputModal from '../GoalInputModal';

// Icon imports
import AddIcon          from '@material-ui/icons/Add';
import CloseIcon        from '@material-ui/icons/Close';
import InfoIcon         from '@material-ui/icons/Info';
import LockIcon         from '@material-ui/icons/Lock';
import LockOpenIcon     from '@material-ui/icons/LockOpen';
import StarIcon         from '@material-ui/icons/Star';

import { RadialChart, DiscreteColorLegend } from 'react-vis';
import { blue }    from '@material-ui/core/colors';
import AppBar             from "@material-ui/core/AppBar/AppBar";
import Avatar             from "@material-ui/core/Avatar/Avatar";
import Button             from "@material-ui/core/Button/Button";
import CircularProgress   from "@material-ui/core/CircularProgress";
import Dialog             from "@material-ui/core/Dialog/Dialog";
import DialogActions      from "@material-ui/core/DialogActions/DialogActions";
import DialogContent      from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText  from "@material-ui/core/DialogContentText/DialogContentText";
import DialogTitle        from "@material-ui/core/DialogTitle/DialogTitle";
import Divider            from "@material-ui/core/Divider/Divider";
import Icon               from '@material-ui/core/Icon';
import IconButton         from '@material-ui/core/IconButton';
import LinearProgress     from '@material-ui/core/LinearProgress';
import List               from "@material-ui/core/List/List";
import ListItem           from "@material-ui/core/ListItem/ListItem";
import ListItemText       from "@material-ui/core/ListItemText/ListItemText";
import { lighten, withStyles, makeStyles, createMuiTheme } from '@material-ui/core/styles';
import MaterialTable      from "material-table";
import Snackbar           from "@material-ui/core/Snackbar";
import SnackbarContent    from '@material-ui/core/SnackbarContent';
import Toolbar            from "@material-ui/core/Toolbar";
import Tooltip            from '@material-ui/core/Tooltip';
import Typography         from "@material-ui/core/Typography";
import { ThemeProvider }  from "@material-ui/styles";

import { Day, Calendar, MuiPickersUtilsProvider } from "@material-ui/pickers";

import MomentUtils        from "@date-io/moment";
import moment             from "moment";
import AddGoal from "../AddGoal";

import {
    FacebookIcon,
    FacebookShareButton,
    TwitterIcon,
    TwitterShareButton,
} from 'react-share';

const useStyles = makeStyles(theme => ({
  root: {
    width: '60%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  info: {
    backgroundColor: blue[300],
  },
  button : {
    border: 0,
    margin: 0,
    display: 'inline-flex',
    outline: 0,
    position: 'relative',
    alignItems: 'center',
    userSelect: 'none',
    verticalAlign: 'middle',
    justifyContent: 'center',
    textDecoration: 'none',
    flex: '0 0 auto',
    padding: '12px',
    overflow: 'visible',
    transition: 'background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    borderRadius: '50%',
    fontSize: '0.875rem',
    textAlign: 'center',
    color: '#fff',
    fontWeight: 500,
  }
}));

const snackbarVariantIcon = {
    info: InfoIcon,
};

const HabitProgress = withStyles({
  root: {
    height: 10,
    borderRadius: 20,
    backgroundColor: lighten('#3f51b5', 0.5),
  },
  bar: {
    borderRadius: 20,
    backgroundColor: '#3f51b5',
  },
})(LinearProgress);

const daysFromMonth = (progress, month) => {
  let days = [];
  const progressArray = Object.keys(progress);
  if (progressArray.length === 0) return [];
  progressArray.forEach(progDay => {
    const momentProgDay = moment(progDay);
    if (momentProgDay.month() === month && progress[progDay]){
      days.push(momentProgDay.date());
    }
  })
  return days;
}

function ProgressConfirmation(props) {
  const { onClose, open, selectedGoal, date, setBusyLogging } = props;
  const logDate = new Date(date.year(), date.month(), date.date());

  let logStatus = true;
  if (selectedGoal.progress[logDate]){
    logStatus = false;
  }

  async function logProgressDate(goalId) {
      try {
          setBusyLogging(true);
          await Firebase.addGoalProgressDate(
            goalId,
            logStatus,
            logDate);
          selectedGoal.progress[logDate] = logStatus;
          setBusyLogging(false);
      } catch (e) {
          alert(e)
      }
  }

  const handlePopupClose = (logProgress) => {
    if (logProgress){
      logProgressDate(selectedGoal.id)
    }
    onClose();
  };

  return (
    <Dialog onClose={() => handlePopupClose(false)} aria-labelledby="progress-confirmation-title" open={open}>
      <DialogTitle id="progress-confirmation-title">{logStatus ? "Logging" : "Removing"} Progress...</DialogTitle>
      <DialogContent>
        <DialogContentText id="progress-confirmation-description">
          {logStatus ? 'Log Progress' : 'Remove Progress' } for <b>{moment(logDate).format("dddd, MMMM Do YYYY").toString()}</b>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handlePopupClose(false)} color="primary">
          Cancel
        </Button>
        <Button onClick={() => handlePopupClose(true)} color="primary" autoFocus>
          {logStatus ? 'Log Progress' : 'Remove Progress' }
        </Button>
      </DialogActions>
    </Dialog>
  );
}

ProgressConfirmation.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedGoal: PropTypes.object.isRequired,
  date: PropTypes.instanceOf(moment),
  setBusyLogging: PropTypes.func.isRequired,
};

const Goals = () => {
    document.title = "My Goals | Elevator Goals";

    // Goal modal state
    const [editModalOpen, setEditModalOpen] = useState(false);
    const [viewingGoal, setViewingGoal] = useState(false);
    const [changingVisibility, setChangingVisibility] = useState(false);
    const [deletingGoal, setDeletingGoal] = useState(false);
    const [selectedGoal, setSelectedGoal] = useState({
        title: '',
        description: '',
        question: '',
        minTask: '',
        category: '',
        public: true,
        id: '',
        progress: [],
        motivationOne: '',
        motivationTwo: '',
        motivationThree: '',
    });
    const [goalToLogProgressFor, setGoalToLogProgressFor] = useState(null);
    const [busyLogging, setBusyLogging] = useState(false);
    const [snackbarStatus, setSnackbarStatus] = useState(null);
    const [snackbarMessage, setSnackbarMessage] = useState("");
    const [openProgressConfirmation, setOpenProgressConfirmation] = useState(false);
    const [calendarDate, setCalendarDate] = useState(moment(new Date()));

    const handleCalendarLog = (date) => {
      const currentDate = moment(new Date());
      if (!date.isAfter(currentDate)){
        setCalendarDate(date);
        setOpenProgressConfirmation(true);
      }
    };

    const handleCalendarLogClose = () => {
      setOpenProgressConfirmation(false);
    };


    const updateGoal = goalId => {
        return async goalInputValues => {
            try {
                await Firebase.updateGoal(goalId, goalInputValues).then(() => goalSnackbar("updated"));
            } catch (e) {
                alert('error with adding goal', e);
            }
        };
    };

    async function logProgress(successful) {
        try {
            setBusyLogging(true);
            await Firebase.addGoalProgress(goalToLogProgressFor.id, successful);
            setBusyLogging(false);
            setGoalToLogProgressFor(null);
        } catch (e) {
            alert(e)
        }
    }

    // Habit Strength
    // Amount of times completed * by overall success
    const classes = useStyles();
    const habitStrengthFromProgress = (creationDate, progress) => {
      const v = Firebase.progressToArray(creationDate, progress);
      const successes = v.filter(x => x).length;
      const proportion = v.length > 0 ? successes / v.length : 0;
      const timesCompleted = Object.entries(progress).filter(prog => prog[1]).length;
      const habit = timesCompleted * proportion;
      const result = (habit/40)*100;
      return result;
    }

    const getProgressData = (creationDate, progress) => {
        const v = Firebase.progressToArray(creationDate, progress);
        const successes = v.filter(x => x).length;
        const data = [{angle: successes, color: "#0000FF"},
                      {angle: v.length - successes, color: "#FF0000"}];
        const proportion = 100 * (v.length > 0 ? successes / v.length : 0);
        return {data, proportion};
    };

    const categoryIconMap = {};
    Firebase.useGoalCategories().forEach(category => {
        categoryIconMap[category.name] = category.icon;
    });

    // Calendar
    moment.locale("en");

    const defaultCalDayTheme = createMuiTheme({
      overrides: {
        MuiPickersDay: {
          daySelected: {
            backgroundColor: "green",
          },
          dayDisabled: {
            color: "black",
          },
        },
      },
    });

    const altCalDayTheme = createMuiTheme({
      overrides: {
        MuiPickersDay: {
          daySelected: {
            backgroundColor: "#3f51b5",
          },
          dayDisabled: {
            color: "black",
          },
        },
      },
    });

    const combinedCalDayTheme = createMuiTheme({
      overrides: {
        MuiPickersDay: {
          daySelected: {
            background: "linear-gradient(90deg, #3f51b5 50%, green 50%)",
          },
          dayDisabled: {
            color: "black",
          },
        },
      },
    });

    // Snackbar
    function SnackbarWrapper(props) {
        const {message, variant, ...other} = props;
        const Icon = snackbarVariantIcon[variant];

        return (
            <SnackbarContent
                className={classes[variant]}
                message={
                    <span id="snackbar" style={{display: 'flex', alignItems: 'center'}}>
                    <Icon style={{paddingRight: "15px"}}/>
                    {message}
                </span>
                }
                action={[
                    <IconButton key="close" aria-label="close" color="inherit" onClick={() => setSnackbarStatus(null)}>
                        <CloseIcon/>
                    </IconButton>,
                ]}
                {...other}
            />
        );
    }

    const goalSnackbar = variant => {
        const mapping = {
            "deleted": {
                "variant": "info",
                "message": "Goal deleted.",
            },
            "updated": {
                "variant": "info",
                "message": "Goal updated!",
            },
            "logged": {
                "variant": "info",
                "message": "Logged your progress!",
            }
        }

        setSnackbarStatus(mapping[variant].variant);
        setSnackbarMessage(mapping[variant].message);
    };

    function socialStreakMessage(goal) {
      const streakDays = Firebase.getStreakDays(goal.progress);
      const dayString = streakDays === 1 ? 'day' : 'days';
      const showGoalTitle = goal.public ? ` ${goal.title}` : '';
      const streakMessage = `I'm on a streak of ${streakDays} ${dayString} for my goal${showGoalTitle}!`;

      const websiteMesssage = 'Elevate your goal tracking with Elevator.';

      return `${streakMessage} ${websiteMesssage}`;
    }

    function streakStringFromGoal (goal){
      const streakDays = Firebase.getStreakDays(goal.progress);
      const dayString = streakDays === 1 ? 'day' : 'days';
      return `${streakDays} ${dayString} 🔥`
    }

    const rawGoals = Firebase.useGoals();
    const goalData = rawGoals.filter(g => g !== null).map(goal => {
        const streakDays = Firebase.getStreakDays(goal.progress);
        const streakString = `Current streak is ${streakDays} ${streakDays === 1 ? 'day' : 'days'}`;

        return {
            ...goal,
            streak: streakDays,
            streakString: streakString,
        }
    });

    return (
        <div>
            {snackbarStatus &&
                <Snackbar open={!!snackbarStatus}
                          autoHideDuration={5000}
                          onClose={() => setSnackbarStatus(null)}
                          anchorOrigin={{'vertical': 'top', 'horizontal': 'center'}}
                >
                    <SnackbarWrapper variant={snackbarStatus} message={snackbarMessage} />
                </Snackbar>
            }

            <h1>My Goals</h1>
            <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '12px' }}>
                <p>Click on a goal to view its details.</p>
                <AddGoal />
            </div>


            {/*<Paper>*/}

            <div style={{maxWidth: '100%'}}>
                {
                    rawGoals.includes(null) ?
                        <div style={{ display: 'flex', alignItems: "center", justifyContent: "center", paddingTop: 200}}>
                            <CircularProgress size={100}/>
                        </div> :
                        <MaterialTable
                            columns={[
                                {title: 'Category', field: 'category', render: rowData => // eslint-disable-line react/display-name
                                    <Tooltip title={rowData.category}>
                                        <Avatar className={rowData.category}>
                                        {/* Render icon for category if available, otherwise use StarIcon */}
                                        {
                                            (rowData.category != null && categoryIconMap[rowData.category] != null) ?
                                                <Icon>{categoryIconMap[rowData.category]}</Icon>
                                                :
                                                <StarIcon />
                                        }
                                        </Avatar>
                                    </Tooltip>
                                },
                                {title: 'Goal', field: 'title', render: rowData => // eslint-disable-line react/display-name
                                    <span style={{color: 'blue', textDecoration: 'underline'}}>{rowData.title}</span>
                                },
                                {title: 'Description', field: 'description'},
                                {title: 'Streak Progress', field: 'streak', disableClick: true, defaultSort: 'desc', render: rowData => // eslint-disable-line react/display-name
                                    <>
                                        <Tooltip title={rowData.streakString}>
                                            <span style={{cursor: 'default'}} edge="end" aria-label="streak">
                                                {rowData.streak} 🔥
                                            </span>
                                        </Tooltip>
                                        <Tooltip title="Log progress">
                                            <IconButton edge="end" aria-label="add" onClick={() => {
                                                setGoalToLogProgressFor(rowData)
                                            }}>
                                                <AddIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </>
                                },
                                {title: 'Visibility', field: 'visibility', disableClick: true, render: rowData => // eslint-disable-line react/display-name
                                    <Tooltip title={rowData.public ? "Public" : "Private"}>
                                        <IconButton onClick={() => {
                                            setSelectedGoal(rowData);
                                            setChangingVisibility(true);
                                            }}>{rowData.public ? <LockOpenIcon/> : <LockIcon/>}
                                        </IconButton>
                                    </Tooltip>
                                },
                            ]}
                            data={goalData}
                            actions={[
                                {
                                    icon: 'edit',
                                    tooltip: 'Edit goal',
                                    onClick: (event, rowData) => {
                                        setSelectedGoal(rowData);
                                        setEditModalOpen(true);
                                    }
                                },
                                {
                                    icon: 'delete',
                                    tooltip: 'Delete goal',
                                    onClick: (event, rowData) => {
                                        setSelectedGoal(rowData);
                                        setDeletingGoal(true);
                                    }
                                }
                            ]}
                            options={{
                                actionsColumnIndex: -1,
                            }}
                            onRowClick={(event, rowData) => {
                                setSelectedGoal(rowData);
                                console.log(event.target);
                                setViewingGoal(true);
                            }}
                            title=""
                        />
                }
            </div>

            <br/>
            { goalToLogProgressFor ? (
                <Dialog
                    open={goalToLogProgressFor}
                    onClose={() => setGoalToLogProgressFor(null)}
                    aria-labelledby="Log progress"
                >
                    {
                        busyLogging ? (
                            <CircularProgress size={50}/>
                        ) : (
                            <>
                                <DialogTitle>
                                    Log progress
                                </DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Did you <b>{goalToLogProgressFor.minTask}</b>?
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={() => setGoalToLogProgressFor(null)} color="primary">
                                        Cancel
                                    </Button>
                                    <Button onClick={() => logProgress(false)} color="primary">
                                        No
                                    </Button>
                                    <Button onClick={() => logProgress(true).then(() => goalSnackbar("logged"))} color="primary" autoFocus>
                                        Yes
                                    </Button>
                                </DialogActions>
                            </>
                        )
                    }
                </Dialog>
            ) : null }

            { viewingGoal ? (
                <Dialog
                    open={viewingGoal}
                    fullWidth
                    onClose={() => setViewingGoal(false)}
                >
                    <AppBar style={{ position: 'relative' }}>
                        <Toolbar>
                            <IconButton edge="start" color="inherit" onClick={() => setViewingGoal(false)} aria-label="close">
                                <CloseIcon />
                            </IconButton>
                            <Typography variant="h6" style={{ marginLeft: 10, flex: 1}}>
                                {selectedGoal.title}
                            </Typography>
                            <Tooltip title="Share to Facebook">
                                <div>
                                    <FacebookShareButton url={"https://elevator-4bb03.web.app/"} quote={socialStreakMessage(selectedGoal)}>
                                        <FacebookIcon size={32}/>
                                    </FacebookShareButton>
                                </div>
                            </Tooltip>
                            <Tooltip title="Share to Twitter">
                                <div>
                                    <TwitterShareButton url={"https://elevator-4bb03.web.app/"} title={socialStreakMessage(selectedGoal)}>
                                        <TwitterIcon size={32}/>
                                    </TwitterShareButton>
                                </div>
                            </Tooltip>
                        </Toolbar>
                    </AppBar>
                    <List>

                        <ListItem button>
                            <ListItemText primary="Daily action" secondary={selectedGoal.minTask} />
                        </ListItem>
                        <Divider/>
                        <ListItem button>
                            <ListItemText primary="Streak" secondary={streakStringFromGoal(selectedGoal)} />
                        </ListItem>
                        <Divider/>
                        <Tooltip title={
                          <span><b>{'(Success Ratio x Streak Days)/40'}</b>
                            <br />
                            <em>{"It takes 40 days to make a habit!"}</em>
                          </span>
                        }>
                          <ListItem button>
                              <ListItemText primary="Habit Strength"
                                secondary={`${habitStrengthFromProgress(
                                selectedGoal.creationDate,
                                selectedGoal.progress).toFixed(1)}%`}/>

                              <div className={classes.root}>
                                <HabitProgress variant="determinate"
                                  value={habitStrengthFromProgress(
                                    selectedGoal.creationDate,
                                    selectedGoal.progress)}
                                  color="secondary"
                                />
                              </div>
                          </ListItem>
                        </Tooltip>
                        <Divider />
                        <ListItem>
                            <ListItemText primary="Overall Success" secondary={`${getProgressData(selectedGoal.creationDate, selectedGoal.progress).proportion}%`} />
                            <RadialChart
                                data={getProgressData(selectedGoal.creationDate, selectedGoal.progress).data}
                                radius={60}
                                innerRadius={50}
                                width={150}
                                height={150}
                                colorType="literal"
                            />
                            <DiscreteColorLegend
                                items={[{title: "Success", color: "#3f51b5"},
                                        {title: "Failure", color: "#FF0000"}]}
                            />
                        </ListItem>
                        <Divider/>
                          <div className="MuiListItem-root MuiListItem-gutters" style={{'textAlign': 'center'}}>
                            <div className="MuiListItemText-root MuiListItemText-multiline">
                              <span className="MuiTypography-root MuiListItemText-primary MuiTypography-body1">
                                Calendar
                              </span>
                              <p className="MuiTypography-root MuiListItemText-secondary MuiTypography-body2 MuiTypography-colorTextSecondary">
                                View and edit previous progress
                              </p>
                              <p className="MuiTypography-root MuiListItemText-secondary MuiTypography-body2 MuiTypography-colorTextSecondary">
                                <span className={classes.button}
                                  style={{'backgroundColor': '#3f51b5'}}>
                                </span>
                                &nbsp; Progress &nbsp;
                                <span className={classes.button}
                                  style={{'backgroundColor': 'green'}}>
                                </span>
                                &nbsp; Today
                              </p>
                            </div>
                          </div>
                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
                              <Fragment>
                                <ThemeProvider theme={defaultCalDayTheme}>
                                  <Calendar
                                    date={moment(new Date())}
                                    onChange={(date) => {handleCalendarLog(date)}}
                                    renderDay={(day, selectedDate, isInCurrentMonth, dayComponent) => {
                                      const selectedDays = daysFromMonth(selectedGoal.progress, day.month());
                                      const isSelected = isInCurrentMonth && selectedDays.includes(day.date());

                                      let resTheme;
                                      let resSelect, isDisabled = false;
                                      if (isSelected && !day.isSame(selectedDate)){
                                        resTheme = altCalDayTheme;
                                        resSelect = true;
                                      } else if (isSelected && day.isSame(selectedDate)){
                                        resTheme = combinedCalDayTheme;
                                        resSelect = true;
                                      } else {
                                        resTheme = defaultCalDayTheme;
                                      }

                                      if (dayComponent.props.hidden || day.isAfter(selectedDate)){
                                        isDisabled = true;
                                      }
                                      return (<ThemeProvider theme={resTheme}>
                                        <Day
                                          selected={resSelect}
                                          disabled={isDisabled}>
                                          {dayComponent}
                                        </Day>
                                      </ThemeProvider>);
                                    }}
                                  />
                                  {openProgressConfirmation ?
                                    <ProgressConfirmation
                                      open={openProgressConfirmation}
                                      onClose={handleCalendarLogClose}
                                      selectedGoal={selectedGoal}
                                      date={calendarDate}
                                      setBusyLogging={setBusyLogging} /> : null}
                                </ThemeProvider>
                              </Fragment>
                            </MuiPickersUtilsProvider>
                    </List>

                </Dialog>
            ) : null }

            { changingVisibility ? (
                <Dialog
                    open={changingVisibility}
                    fullWidth
                    onClose={() => setChangingVisibility(false)}
                >
                    <DialogTitle>
                        Change Goal Visibility
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {"Are you sure? Changing visibility from"} <strong>{selectedGoal.public ? 'Public' : 'Private'}</strong> to <strong>{selectedGoal.public ? 'Private' : 'Public'}.</strong>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setChangingVisibility(false)} color="primary">
                            No
                        </Button>
                        <Button
                            onClick={
                                () => {
                                    setChangingVisibility(false);
                                    updateGoal(selectedGoal.id)({public: !selectedGoal.public});
                                }
                            }
                            color="primary" autoFocus
                        >
                            Yes
                        </Button>
                    </DialogActions>
                </Dialog>
            ) : null}

            { deletingGoal ? (
                <Dialog
                    open={deletingGoal}
                    fullWidth
                    onClose={() => setDeletingGoal(false)}
                >
                    <DialogTitle>
                        Delete goal
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Are you sure you want to delete <strong>{selectedGoal.title}</strong>?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setDeletingGoal(false)} color="primary">
                            No
                        </Button>
                        <Button
                            onClick={
                                () => {
                                    setDeletingGoal(false);
                                    Firebase.deleteGoal(selectedGoal.id)
                                        .then(() => goalSnackbar("deleted"));
                                }
                            }
                            color="primary" autoFocus>
                            Yes
                        </Button>
                    </DialogActions>
                </Dialog>
            ) : null }

            <GoalInputModal
                modalTitle="Edit goal"
                saveTitle="Save changes"
                initialGoalInputValues={selectedGoal}
                isOpen={editModalOpen}
                saveAction={updateGoal(selectedGoal.id)}
                setIsOpen={setEditModalOpen} />
        </div>

    );
};

Goals.propTypes = {
    tab: PropTypes.number,
    userName: PropTypes.string,
    message: PropTypes.string,
    variant: PropTypes.string,
};

export default Goals;
