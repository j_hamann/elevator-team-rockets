import React, {useState} from "react";
import Firebase          from "../../../firebase_setup";
import PropTypes         from "prop-types";

import Button            from "@material-ui/core/Button";
import DialogActions     from "@material-ui/core/DialogActions";
import DialogContent     from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle       from "@material-ui/core/DialogTitle";
import FormControl       from "@material-ui/core/FormControl";
import FormHelperText    from "@material-ui/core/FormHelperText";
import Input             from "@material-ui/core/Input";
import InputLabel        from "@material-ui/core/InputLabel";
import MenuItem          from "@material-ui/core/MenuItem";
import Select            from "@material-ui/core/Select";


// handles goal name, desc, category
const StepTwo = (props) => {
    const { handleReset, handleNext } = props;

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState(null);

    const categories = Firebase.useGoalCategories();


    const finishStepTwo = () => {
        handleNext({title, description, category})
    };


    return (
        <>
            <DialogTitle>New Goal</DialogTitle>

            <DialogContent>
              <DialogContentText color='textPrimary'>
                Specify the overall goal you are aiming towards. Try to think of something that&#39;s manageable and can be broken down into small, regular steps.
              </DialogContentText>

              <FormControl fullWidth margin="dense">
                <InputLabel htmlFor="goal-title">Goal name</InputLabel>
                <Input
                  required
                  autoFocus
                  id="goal-title"
                  value={title}
                  onChange={(t) => setTitle(t.target.value)}
                  aria-describedby="goal-title-helpertext"
                />
                <FormHelperText id="goal-title-helpertext">What are you trying to achieve?</FormHelperText>
              </FormControl>

              <FormControl fullWidth margin="dense">
                <InputLabel htmlFor="goal-description">Description</InputLabel>
                <Input
                  required
                  id="goal-description"
                  value={description}
                  onChange={(t) => setDescription(t.target.value)}
                  aria-describedby="goal-description-helpertext"
                />
                <FormHelperText id="goal-description-helpertext">What is the purpose of the goal, or why is this important to you?</FormHelperText>
              </FormControl>

              <FormControl fullWidth margin="dense">
                <InputLabel htmlFor="goal-category">Category</InputLabel>
                <Select
                  required
                  id="goal-category"
                  value={category}
                  onChange={(t) => setCategory(t.target.value)}
                  style={{ minWidth: 120}}
                  aria-describedby="goal-category-helpertext"
                >
                  {categories.map((category) =>
                      (<MenuItem key={category.id} value={category.name}>{category.name}</MenuItem>)
                  )}
                </Select>
                <FormHelperText id="goal-category-helpertext">What area of your life does this goal relate to?</FormHelperText>
              </FormControl>
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleReset}
                >
                    Reset
                </Button>
                <Button variant="contained" disabled={!(title && description && category)} color="primary" onClick={finishStepTwo}>
                    Next
                </Button>
            </DialogActions>

        </>
    );
};


StepTwo.propTypes = {
    handleReset: PropTypes.func,
    handleNext: PropTypes.func,
};



export default StepTwo;
