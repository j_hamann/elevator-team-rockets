import React from "react";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import PropTypes from "prop-types";
import DialogTitle from "@material-ui/core/DialogTitle";


const StepOne = (props) => {
    const { handleNext } = props;


    return (
        <>
            <DialogTitle>New Goal</DialogTitle>
            <DialogContent>
                <DialogContentText color='textPrimary'>Goal setting is hard. We&#39;re trying to make it easier. Follow our goal creation flow to:</DialogContentText>
                <DialogContentText>Breakdown your goal</DialogContentText>
                <DialogContentText>Reflect on your motivations</DialogContentText>
                <DialogContentText>Breakdown your goal into the first step</DialogContentText>
                <DialogContentText>How to stay on track</DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button
                    disabled
                >
                    Reset
                </Button>
                <Button variant="contained" color="primary" onClick={() => handleNext({})}>
                    Next
                </Button>
            </DialogActions>
        </>
    );
};


StepOne.propTypes = {
    handleReset: PropTypes.func,
    handleNext: PropTypes.func
}

export default StepOne;
