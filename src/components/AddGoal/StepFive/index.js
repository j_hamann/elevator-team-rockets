import React, {useState} from "react";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import PropTypes from "prop-types";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Switch from "@material-ui/core/Switch/Switch";
import '../../Styles/_box-layout.css';

const StepFive = (props) => {
    const { handleReset, handleNext } = props;


    const [goalPublic, setGoalPublic] = useState(false);

    const finishStepFive = () => {
        handleNext({
            public: goalPublic
        })
    };

    return (
        <>
        <DialogTitle>Join the party <span role='img' aria-label="public goal">🎉</span></DialogTitle>
            <DialogContent>
                <DialogContentText color='textPrimary'>Did you know that you are 74% more likely to achieve your goal if you announce it to the public?</DialogContentText>
                <br/>

                <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <DialogContentText color='textPrimary' style={{margin: 0, paddingRight: 10}}>
                        Join the public leaderboard?
                    </DialogContentText>

                    <FormControlLabel
                        control={
                            <Switch
                                checked={goalPublic}
                                onChange={() => setGoalPublic(!goalPublic)}
                                value="checkedB"
                                color="primary"
                            />
                        }
                    />
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleReset}>
                    Reset
                </Button>
                <Button variant="contained" color="primary" onClick={finishStepFive}>
                    Finish
                </Button>
            </DialogActions>
        </>
    );
};


StepFive.propTypes = {
    handleReset: PropTypes.func,
    handleNext: PropTypes.func
};

export default StepFive;
