import React from 'react';
import {storiesOf} from "@storybook/react";
import StepFive from "./index";
import {noop} from "@babel/types";


storiesOf('AddGoal', module)
    .add('Step Five', () => (
        <StepFive
            handleNext={noop}
            handleReset={noop}
        />
        )
    );
