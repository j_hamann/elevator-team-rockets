import React, {useState} from "react";
import PropTypes from 'prop-types';

import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import TextField from "@material-ui/core/TextField/TextField";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";


const StepThree = (props) => {

    const { handleReset, handleNext } = props;

    const [motivationOne, setMotivationOne] = useState('');
    const [motivationTwo, setMotivationTwo] = useState('');
    const [motivationThree, setMotivationThree] = useState('');


    const finishStepThree = () => {
        console.log(motivationOne, motivationThree, motivationTwo)
        handleNext({motivationOne, motivationTwo, motivationThree})
    };

    return (
        <>
            <DialogTitle>What&#39;s your motivation?</DialogTitle>

            <DialogContent>
                <DialogContentText color='textPrimary'>Goal setting is damn hard. Take a moment to think about this goal and you motivations for doing it, then fill out the three whys below.</DialogContentText>
                <InputLabel>Why?</InputLabel>
                <TextField
                    autoFocus
                    required
                    fullWidth
                    margin="dense"
                    id="motivationOne"
                    value={motivationOne}
                    onChange={(t) => setMotivationOne(t.target.value)}
                />

                <InputLabel>Why?</InputLabel>
                <TextField
                    required
                    fullWidth
                    margin="dense"
                    id="motivationTwo"
                    value={motivationTwo}
                    onChange={(t) => setMotivationTwo(t.target.value)}
                />

                <InputLabel>No, really why?</InputLabel>
                <TextField
                    required
                    fullWidth
                    margin="dense"
                    id="motivationThree"
                    value={motivationThree}
                    onChange={(t) => setMotivationThree(t.target.value)}
                />

            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleReset}
                >
                    Reset
                </Button>
                <Button variant="contained" disabled={ !(motivationOne && motivationTwo && motivationThree) } color="primary" onClick={finishStepThree}>
                    Next
                </Button>
            </DialogActions>


        </>
    );
};

StepThree.propTypes = {
    handleReset: PropTypes.func,
    handleNext: PropTypes.func
};


export default StepThree;
