import React, {useState} from "react";
import PropTypes from 'prop-types';

import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import TextField from "@material-ui/core/TextField/TextField";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";


const StepFour = (props) => {

    const { handleReset, handleNext } = props;

    const [minTask, setMinTask] = useState('');


    const finishStepFour = () => {
        handleNext({minTask, question: `Did you "${minTask}" today?`})
    };

    return (
        <>
            <DialogTitle>Lets build the habit<span role='img' aria-label="fire">🔥</span></DialogTitle>

            <DialogContent>
                <DialogContentText color='textPrimary'>Break your goal down into the first step. By doing this minimal action each day, you will be amazed by how quickly you can build strong, lasting habits.</DialogContentText>
                <DialogContentText color='textPrimary' style={{fontStyle: 'italic'}}>For example: if you goal is to get fit, your minimal action could be to wake up and put you running shoes on.</DialogContentText>
                <DialogContentText color='textPrimary'>This is the habit you will be tracking daily!</DialogContentText>

                <InputLabel>What is your daily minimal action?</InputLabel>
                <TextField
                    autoFocus
                    required
                    fullWidth
                    margin="dense"
                    id="minTask"
                    value={minTask}
                    onChange={(t) => setMinTask(t.target.value)}
                />

            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleReset}
                >
                    Reset
                </Button>
                <Button variant="contained" disabled={ !(minTask) } color="primary" onClick={finishStepFour}>
                    Next
                </Button>
            </DialogActions>

        </>
    );
};

StepFour.propTypes = {
    handleReset: PropTypes.func,
    handleNext: PropTypes.func
};


export default StepFour;
