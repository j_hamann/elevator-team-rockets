import React, {useState, useEffect} from "react";
import Firebase from '../../firebase_setup';

import PropTypes from 'prop-types';

import StepOne   from "./StepOne";
import StepTwo   from "./StepTwo";
import StepThree from "./StepThree";
import StepFour  from "./StepFour";
import StepFive  from "./StepFive";

import Button           from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Dialog           from "@material-ui/core/Dialog";
import Divider          from "@material-ui/core/Divider";
import Hidden           from "@material-ui/core/Hidden";
import IconButton       from '@material-ui/core/IconButton';
import MuiDialogTitle   from "@material-ui/core/DialogTitle";
import Snackbar         from "@material-ui/core/Snackbar";
import SnackbarContent  from '@material-ui/core/SnackbarContent';
import Step             from "@material-ui/core/Step";
import StepLabel        from "@material-ui/core/StepLabel";
import Stepper          from "@material-ui/core/Stepper";
import Tooltip          from '@material-ui/core/Tooltip';
import Typography       from '@material-ui/core/Typography';

import { makeStyles }       from "@material-ui/core/styles";
import { green }            from '@material-ui/core/colors';
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import CheckCircleIcon      from '@material-ui/icons/CheckCircle';
import CloseIcon            from '@material-ui/icons/Close';

import {
    FacebookIcon,
    FacebookShareButton,
    TwitterIcon,
    TwitterShareButton,
} from 'react-share';


const AddGoal = (props) => {

    const FINAL_STEP = 5;

    const [isOpen, setIsOpen] = useState(false);
    const [goalValues, setGoalValues] = useState({});
    const [snackbarGoalName, setSnackbarGoalName] = useState(null);

    const submitGoal = async (goalInputValues) => {
        try {
            await Firebase.addGoal(goalInputValues);
        } catch (e) {
            alert('error with adding goal', e);
        }
    };

    const [activeStep, setActiveStep] = React.useState(0);

    const handleNext = (values) => {
        setGoalValues({
            ...values,
            ...goalValues,
        });
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    const handleClose = () => {
        setIsOpen(false);
        setActiveStep(0);
    };

    // Finish goal creation
    useEffect(() => {
        async function finishGoalCreation() {
            if (activeStep === FINAL_STEP) {
                setActiveStep(prevActiveStep => prevActiveStep + 1)
                await submitGoal(goalValues)
                    .then(() => setSnackbarGoalName(goalValues.title));

                handleReset();
                setGoalValues({});
                setIsOpen(false);
            }
        }
        finishGoalCreation();
    }, [activeStep, goalValues]);

    function getSteps() {
        return ['Goal overview', 'Goal details', 'Motivations', 'Minimum Action', 'Make it public!'];
    }

    function getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return <StepOne handleNext={handleNext} handleReset={handleReset}/>;
            case 1:
                return <StepTwo handleNext={handleNext} handleReset={handleReset}/>;
            case 2:
                return <StepThree handleNext={handleNext} handleReset={handleReset}/>;
            case 3:
                return <StepFour handleNext={handleNext} handleReset={handleReset}/>;
            case 4:
                return <StepFive handleNext={handleNext} handleReset={handleReset}/>;
            default:
                return (
                    <div style={{ display: 'flex', alignItems: "center", justifyContent: "center" }}>
                        <CircularProgress size={100}/>
                    </div>
                );
        }
    }

    const useStyles = makeStyles(theme => ({
        button: {
            margin: theme.spacing(1),
            alignItems: 'center',
            justifyContent: 'center',
        },
        title: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
        },
    }));
    const classes = useStyles();

    function socialCreatedMessage() {
        const createdMessage = `I just started tracking a new goal!`;
        const websiteMessage = 'Elevate your goal tracking with Elevator.';

        return `${createdMessage} ${websiteMessage}`;
    }


    return (
        <>
            { props.small ?
              <Button onClick={() => setIsOpen(true)} variant="contained" color="secondary" className={classes.button} title="Add Goal">
                <AddCircleOutlineIcon />
              </Button>
              :
              <Button onClick={() => setIsOpen(true)} variant="contained" color="secondary" startIcon={<AddCircleOutlineIcon />} className={classes.button}>
                  Add Goal
              </Button>
            }

            <Dialog open={isOpen} aria-labelledby="Add-Goal" maxWidth='md' fullWidth>
                <MuiDialogTitle disableTypography className={classes.title}>
                  <Typography variant="h6">Let&apos;s start your next goal!</Typography>
                  <IconButton onClick={handleClose} className={classes.closeButton} aria-label="close">
                    <CloseIcon />
                  </IconButton>
                </MuiDialogTitle>

                <Hidden only={['xs', 'md']}>
                    <Stepper activeStep={activeStep} alternativeLabel>
                        {getSteps().map(label => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                </Hidden>

                <Divider />

                {getStepContent(activeStep)}
            </Dialog>

            {snackbarGoalName &&
            <Snackbar open={!!snackbarGoalName}
                      autoHideDuration={5000}
                      onClose={() => setSnackbarGoalName(null)}
                      anchorOrigin={{'vertical': 'top', 'horizontal': 'center'}}
            >
                <SnackbarContent
                    style={{backgroundColor: green[600]}}
                    message={
                        <span id="snackbar" style={{display: 'flex', alignItems: 'center'}}>
                                    <CheckCircleIcon style={{paddingRight: "15px"}}/>
                            {`Your goal "${snackbarGoalName}" was created! Share this on:`}
                            </span>
                    }
                    action={[
                        <Tooltip key="btn-fb-share" title="Share to Facebook">
                            <div>
                                <FacebookShareButton url={"https://elevator-4bb03.web.app/"} quote={socialCreatedMessage()}>
                                    <FacebookIcon size={32}/>
                                </FacebookShareButton>
                            </div>
                        </Tooltip>,
                        <Tooltip key="btn-twitter-share" title="Share to Twitter">
                            <div>
                                <TwitterShareButton url={"https://elevator-4bb03.web.app/"} title={socialCreatedMessage()}>
                                    <TwitterIcon size={32}/>
                                </TwitterShareButton>
                            </div>
                        </Tooltip>,
                        <IconButton key="close" aria-label="close" color="inherit" onClick={() => setSnackbarGoalName(null)}>
                            <CloseIcon/>
                        </IconButton>,
                    ]}
                />
            </Snackbar>
            }

        </>
    );
};

AddGoal.propTypes = {
  small: PropTypes.bool
};

export default AddGoal;
