import React, {useState} from "react";
import Firebase from '../../firebase_setup';
import PropTypes from 'prop-types';
import {Pie, Line} from 'react-chartjs-2';
import StarIcon  from '@material-ui/icons/Star';
import CloseIcon from '@material-ui/icons/Close';

import Avatar              from "@material-ui/core/Avatar/Avatar";
import Button              from '@material-ui/core/Button';
import CircularProgress   from "@material-ui/core/CircularProgress";
import Icon                from '@material-ui/core/Icon';
import IconButton        from "@material-ui/core/IconButton";
import KeyboardArrowLeft   from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight  from '@material-ui/icons/KeyboardArrowRight';
import MaterialTable       from 'material-table';
import MobileStepper       from '@material-ui/core/MobileStepper';
import SwipeableViews      from 'react-swipeable-views';
import Snackbar            from "@material-ui/core/Snackbar";
import Tooltip             from '@material-ui/core/Tooltip';
import Typography          from '@material-ui/core/Typography';

import { autoPlay }                         from 'react-swipeable-views-utils';
import { useTheme } from '@material-ui/core/styles';

import '../Styles/categories.css';
import AddGoal from "../AddGoal";


const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const Dashboard = (props) => {
    document.title = "My Dashboard | Elevator Goals";

    const [open, setOpen] = useState(true);

    const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
      setOpen(false);
    };

    function getUserCatGoals(category) {
        let goals = Firebase.useGoals().filter(g => g !== null);
        let count = 0;
        for (let i = 0, l = goals.length; i < l; i++) {
            if (goals[i].category === category){
              count += Object.keys(goals[i].progress).length;
            }
        }
        return count;
    }

    function hasStreak() {
      let goals = Firebase.useGoals().filter(g => g !== null);
        for (let i = 0, l = goals.length; i < l; i++) {
            if (Firebase.getStreakDays(goals[i].progress) > 0) {
              return true;
            }
        }
        return false;
    }

    function notSignedInToday() {
      const today = new Date();
      const today_day = today.getDate();
      const today_month = today.getMonth() + 1;
      const today_year = today.getFullYear();
      // Firebase.getLastSignInTime().substring(8,11)
      const last_signin_day = parseInt(Firebase.getLastSignInTime().substring(5,7), 10);
      const signin_month = Firebase.getLastSignInTime().substring(8,11);
      const last_signin_year = parseInt(Firebase.getLastSignInTime().substring(12,16), 10);
      let last_signin_month = 0;
      switch(signin_month) {
        case "Jan":
          last_signin_month = 1;
          break;
        case "Feb":
          last_signin_month = 2;
          break;
        case "Mar":
          last_signin_month = 3;
          break;
        case "Apr":
          last_signin_month = 4;
          break;
        case "May":
          last_signin_month = 5;
          break;
        case "Jun":
          last_signin_month = 6;
          break;
        case "Jul":
          last_signin_month = 7;
          break;
        case "Aug":
          last_signin_month = 8;
          break;
        case "Sep":
          last_signin_month = 9;
          break;
        case "Oct":
          last_signin_month = 10;
          break;
        case "Nov":
          last_signin_month = 11;
          break;
        case "Dec":
          last_signin_month = 12;
          break;
        default:
          last_signin_month = 0;
      }

      return !((today_day === last_signin_day)
           && (today_month === last_signin_month)
           && (today_year === last_signin_year))
    }


    const categoryIconMap = {};
    Firebase.useGoalCategories().forEach(category => {
        categoryIconMap[category.name] = category.icon;
    });

    const { userName } = props;
    const num_streaks = Firebase.useGoals().filter(g =>
      g !== null && Firebase.getStreakDays(g.progress) !== 0
    ).length;
    const num_non_streaks = Firebase.useGoals().filter(g =>
      g !== null && Firebase.getStreakDays(g.progress) === 0
    ).length;

    let tableData = Firebase.getDataForDashGraph()
    let tableY = tableData[0]
    let tableX = tableData[1]

    // Motivation Autoscroller
    let motivationList = [];
    Firebase.useGoals().filter(g => g !== null).forEach(g => {
      motivationList.push({
        goalName: g.title,
        motivation: g.motivationOne,
      });

      motivationList.push({
        goalName: g.title,
        motivation: g.motivationTwo,
      });

      motivationList.push({
        goalName: g.title,
        motivation: g.motivationThree,
      });
    });

    const motivationTheme = useTheme();
    const [motivationActiveStep, setMotivationActiveStep] = React.useState(0);
    const maxMotivationSteps = motivationList.length;

    const handleMotivationNext = () => {
      if (motivationActiveStep === maxMotivationSteps - 1){
        setMotivationActiveStep(0);
      } else {
        setMotivationActiveStep(prevActiveStep => prevActiveStep + 1);
      }
    };
    const handleMotivationBack = () => {
      if (motivationActiveStep === 0){
        setMotivationActiveStep(maxMotivationSteps - 1);
      } else {
        setMotivationActiveStep(prevActiveStep => prevActiveStep - 1);
      }
    };
    const handleMotivationStepChange = step => {
      setMotivationActiveStep(step);
    };

    const randomGoalsData = Firebase.getRandomGoals("").map(goal => {
        const streakDays = Firebase.getStreakDays(goal.progress);
        const streakString = `${streakDays} ${streakDays === 1 ? 'day' : 'days'} 🔥`;

        return {
            ...goal,
            streakString,
        };
    });

    return (
        <div>
            <h1>My Dashboard</h1>
            <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '12px' }}>
              <p>Welcome back, {userName}.</p>
              <AddGoal />
            </div>
            <div style={{textAlign: "center", cursor: "default"}}>
              <Typography>{motivationList[motivationActiveStep] ? motivationList[motivationActiveStep].goalName : null}</Typography>
              because...
              <AutoPlaySwipeableViews
                axis={motivationTheme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={motivationActiveStep}
                onChangeIndex={handleMotivationStepChange}
                interval={10000}
                enableMouseEvents
              >
                {motivationList.map((step, index) => (
                  <div key={step.label}>
                    {Math.abs(motivationActiveStep - index) <= 2 ? (
                      <h3>{motivationList[motivationActiveStep].motivation}</h3>
                    ) : null}
                  </div>
                ))}
              </AutoPlaySwipeableViews>
              <MobileStepper
                steps={maxMotivationSteps}
                position="static"
                variant="dots"
                activeStep={motivationActiveStep}
                nextButton={
                  <Button size="small" onClick={handleMotivationNext}>
                    Next
                    {motivationTheme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                  </Button>
                }
                backButton={
                  <Button size="small" onClick={handleMotivationBack}>
                    {motivationTheme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                    Back
                  </Button>
                }
              />
            </div>
            <div>
              <div className="dashboardRow">
                  <div className="dashboardColumn">
                      <h3 style={{textAlign:"center"}}>{num_streaks + num_non_streaks !== 1 ? num_streaks + " of your "  + (num_streaks + num_non_streaks) +  " goals are on a streak!" : num_streaks === 1 ? "Your 1 goal is on a streak!" : "Your goal is not on a streak..."}</h3>
                      <Pie
                        data={{
                          labels: ['Goals on Streaks', 'Broken Streaks'],
                          datasets: [{
                            data: [ num_streaks,
                                    num_non_streaks
                            ],
                            backgroundColor: ['#42ae33', '#5492d5']
                          }]
                        }}
                        width={null}
                        options={{
                          aspectRatio: 1  // this would be a 1:1 aspect ratio
                        }}
                      />
                  </div>
                  <div className="dashboardColumn">
                      <h3 style={{textAlign:"center"}}>What are you making improvements on?</h3>

                      <Pie
                        data={{
                          labels: ['Study', 'Occupation', 'Fitness', 'Music', 'Language'],
                          datasets: [{
                              data: [getUserCatGoals("Study"),
                                     getUserCatGoals("Occupation"),
                                     getUserCatGoals("Fitness"),
                                     getUserCatGoals("Music"),
                                     getUserCatGoals("Language")],
                              backgroundColor: ['#42ae33', '#5492d5', '#e99a2f', '#cc9ed1', '#f74f4f']
                          }]
                        }}
                        width={null}
                        options={{
                          aspectRatio: 1  // this would be a 1:1 aspect ratio
                        }}
                      />
                  </div>
              </div>
            </div>

            <br />
            <div>
              <h2> How many goals have you achieved per day?</h2>
              <Line
                data={{
                  labels: tableY,
                  datasets: [{
                    label: null,
                    data: tableX,
                    backgroundColor: ['lightblue'],
                    borderColor: ['orange']
                  }],
                }}
                legend={null}
              />
            </div>
          <p>
            <h2>What other people are achieving today</h2>
              {randomGoalsData.length === 0 ?
                  <div style={{ display: 'flex', alignItems: "center", justifyContent: "center", paddingTop: 200}}>
                      <CircularProgress size={100}/>
                  </div> :
                  <MaterialTable
                      columns={[
                          {title: 'Category', field: 'category', render: rowData => // eslint-disable-line react/display-name
                                  <Tooltip title={rowData.category}>
                                      <Avatar className={rowData.category}>
                                          {/* Render icon for category if available, otherwise use StarIcon */}
                                          {
                                              (rowData.category != null && categoryIconMap[rowData.category] != null) ?
                                                  (<Icon>{categoryIconMap[rowData.category]}</Icon>)
                                                  :
                                                  (<StarIcon />)
                                          }
                                      </Avatar>
                                  </Tooltip>
                          },
                          {title: 'Name', field: 'ownerName'},
                          {title: 'Title', field: 'title'},
                          {title: 'Description', field: 'description'},
                          {title: 'Streak Progress', field: 'streakString'},
                      ]}
                      data={randomGoalsData}
                      options={{
                        sorting: false,
                        filtering: false,
                        paging: false,
                        search: false,
                        toolbar: false,
                        draggable: false}}
                  />
              }
          </p>
          {notSignedInToday() && hasStreak() &&
            <Snackbar
              anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
              }}
              open={open}
              autoHideDuration={60000}
              onClose={handleClose}
              ContentProps={{
              'aria-describedby': 'message-id',
              }}
              message={<span>Congratulations, you are on a streak!<br /> Remember to keep the flame alive!</span>}
              action={[
                <IconButton
                  key="close"
                  aria-label="close"
                  color="inherit"
                  onClick={handleClose}
                >
                  <CloseIcon />
                </IconButton>,
              ]}
            />
          }
        </div>
    );
};

Dashboard.propTypes = {
    userName: PropTypes.string,
};

export default Dashboard;
