import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route, NavLink, Link} from "react-router-dom";

import Firebase    from './firebase_setup'
import InfoPage    from "./components/InfoPage";
import Goals       from "./components/Goals";
import Leaderboard from "./components/Leaderboard";
import AddGoal     from "./components/AddGoal";
import Profile     from "./components/Profile";
import Dashboard   from "./components/Dashboard";
import About       from "./components/About";

import AppBar            from "@material-ui/core/AppBar";
import Avatar            from '@material-ui/core/Avatar';
import Button            from "@material-ui/core/Button";
import CircularProgress  from "@material-ui/core/CircularProgress";
import Container         from "@material-ui/core/Container";
import IconButton        from "@material-ui/core/IconButton";
import ListItemIcon      from "@material-ui/core/ListItemIcon";
import ListItemText      from "@material-ui/core/ListItemText";
import MenuItem          from '@material-ui/core/MenuItem';
import MenuList          from '@material-ui/core/MenuList';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state'
import Popover           from '@material-ui/core/Popover'
import Toolbar           from "@material-ui/core/Toolbar";
import Typography        from "@material-ui/core/Typography";

// import './App.css';

import { ThemeProvider, createMuiTheme } from "@material-ui/core";
import green from "@material-ui/core/colors/green";
import teal from "@material-ui/core/colors/teal";
import red from "@material-ui/core/colors/red";
import { makeStyles } from '@material-ui/core/styles';

import FormatListNumberedOutlinedIcon from "@material-ui/icons/FormatListNumberedOutlined";
import DashboardIcon from "@material-ui/icons/Dashboard";
import StarsIcon from "@material-ui/icons/Stars";
import MenuIcon from "@material-ui/icons/Menu";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import InfoIcon from '@material-ui/icons/Info';


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  content: {
    backgroundColor: 'rgba(255,255,255,0.95)',
    padding: '16px',
    paddingTop: '64px',
    minHeight: 'calc(100%)',
  },
  toolbar: {
    height: '64px',
  },
  sectionDesktop: {
    alignItems: 'center',
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    alignItems: 'center',
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    margin: '2px',
    '&:hover': {
      backgroundColor: '#0000002C',
      transition: '.3s',
    },
  },
  activeButton: {
    fontWeight: 'bold',
    backgroundColor: '#0000001C',
  },
  activeMenu: {
    backgroundColor: 'rgba(0, 0, 0, 0.14)',
  },
  avatar: {
    cursor: 'pointer',
    margin: '10px',
    color: '#fff',
    filter: 'brightness(1.0)',
    transition: '.3s',
    '&:hover': {
      filter: 'brightness(0.6)',
      transition: '.3s',
    }
  },
}));

function App() {

    const [firebaseInitialised, setFirebaseInitialised] = useState(false);

    // Avatar Defintions
    const classes = useStyles();

    useEffect(() => {
        Firebase.isInitialised().then((val) => {
            setFirebaseInitialised(val)
        })
    });

    async function logout() {
        try {
            await Firebase.signout();
            window.location.reload()
        } catch (e) {
            alert(e.message)
        }
    }

    if (firebaseInitialised !== false)  {
        if (!Firebase.isUserLoggedIn()) {
            return <InfoPage/>;
        }

        const theme = createMuiTheme({
            palette: {
                primary: green,
                secondary: teal,
                error: red,
            }
        });

        const photoURL = Firebase.getPhotoUrl();

        const avatarPopup = (
          <PopupState variant="popover" popupId="avatarButton">
            {popupState => (
              <div>
                <Avatar src={photoURL}
                  className={classes.avatar}
                  title="User management"
                  {...bindTrigger(popupState)}>
                </Avatar>
                <Popover
                  {...bindPopover(popupState)}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                >
                  <MenuList>
                    <MenuItem component={NavLink} to="/profile" activeClassName={classes.activeMenu}>
                      <ListItemIcon><AccountCircleIcon fontSize="small" /></ListItemIcon>
                      <ListItemText primary="Profile" />
                    </MenuItem>
                    <MenuItem onClick={logout}>
                      <ListItemIcon><ExitToAppIcon fontSize="small" /></ListItemIcon>
                      <ListItemText primary="Logout" />
                    </MenuItem>
                  </MenuList>
                </Popover>
              </div>
            )}
          </PopupState>
        );

        const morePopups = inMobile => (
          <PopupState variant="popover" popupId="mainMenu">
            {popupState => (
              <div>
                <IconButton variant="contained" {...bindTrigger(popupState)} title="More..." className={classes.button}>
                  <MenuIcon color="action" />
                </IconButton>
                <Popover
                  {...bindPopover(popupState)}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                >
                  { inMobile ? (
                    <MenuList>
                      <MenuItem component={NavLink} to="/dashboard" activeClassName={classes.activeMenu} onClick={popupState.close}>
                        <ListItemIcon><DashboardIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="Dashboard" />
                      </MenuItem>
                      <MenuItem component={NavLink} to="/goals" activeClassName={classes.activeMenu} onClick={popupState.close}>
                        <ListItemIcon><StarsIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="Goals" />
                      </MenuItem>
                      <MenuItem component={NavLink} to="/leaderboard" activeClassName={classes.activeMenu} onClick={popupState.close}>
                        <ListItemIcon><FormatListNumberedOutlinedIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="Leaderboard" />
                      </MenuItem>
                      <MenuItem component={NavLink} to="/about" activeClassName={classes.activeMenu} onClick={popupState.close}>
                        <ListItemIcon><InfoIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="About" />
                      </MenuItem>
                    </MenuList>
                  ) : (
                    <MenuList>
                      <MenuItem component={NavLink} to="/leaderboard" activeClassName={classes.activeMenu} onClick={popupState.close}>
                        <ListItemIcon><FormatListNumberedOutlinedIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="Leaderboard" />
                      </MenuItem>
                      <MenuItem component={NavLink} to="/about" activeClassName={classes.activeMenu} onClick={popupState.close}>
                        <ListItemIcon><InfoIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="About" />
                      </MenuItem>
                    </MenuList>
                  ) }
                </Popover>
              </div>
            )}
          </PopupState>
        );

        return (
          <div className="app_background">
            <ThemeProvider theme={theme}>
                <Router>
                    <AppBar position="fixed">
                        <Container maxWidth="md" component={Toolbar} className={classes.toolbar}>
                            <Link to="dashboard" style={{flex: 1, textDecoration: 'none'}}>
                                <Typography variant="h6" noWrap>
                                        <span role='img' aria-label="rocket">Elevator 🚀</span>
                                </Typography>
                            </Link>

                            <div className={classes.sectionDesktop}>
                              <AddGoal />

                              <Button color="inherit" component={NavLink}
                                to="/dashboard"
                                className={classes.button}
                                activeClassName={classes.activeButton}
                                startIcon={<DashboardIcon />}
                                title="Dashboard"
                              >
                                  Dashboard
                              </Button>

                              <Button color="inherit" component={NavLink}
                                to="/goals"
                                className={classes.button}
                                activeClassName={classes.activeButton}
                                startIcon={<StarsIcon />}
                                title="Goals"
                              >
                                  Goals
                              </Button>

                              {morePopups(false)}
                              {avatarPopup}
                            </div>
                            <div className={classes.sectionMobile}>
                              <AddGoal small />
                              {morePopups(true)}
                              {avatarPopup}
                            </div>
                        </Container>
                    </AppBar>
                    <Container maxWidth="md" className={classes.content}>
                            <Switch>
                                <Route exact path="/" render={() => <InfoPage username={Firebase.getCurrentUserName()}/>}/>
                                <Route exact path="/about" component={() => <About />}/>
                                <Route exact path="/goals" component={() => <Goals userName={Firebase.getCurrentUserName()} />} />
                                <Route exact path="/leaderboard" component={() => <Leaderboard userName={Firebase.getCurrentUserName()} />}/>
                                <Route exact path="/profile" component={() => <Profile userName={Firebase.getCurrentUserName()} />}/>
                                <Route exact path="/dashboard" component={() => <Dashboard userName={Firebase.getCurrentUserName()} />} />
                            </Switch>
                    </Container>

                </Router>

            </ThemeProvider>
          </div>
        )
    } else {
        // make prettier
        return (
            <div style={{ display: 'flex', alignItems: "center", justifyContent: "center", paddingTop: 200}}>
                <CircularProgress size={100}/>
            </div>
        )

    }
}

export default App;
